package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout();

    Boolean isAuth();

    String getUserId() throws AbstractException;

    User getUser() throws AbstractException;

    void checkRoles(Role[] roles) throws AbstractException;

}
