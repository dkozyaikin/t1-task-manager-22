package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name) throws AbstractException;

    Task create(String userId, String name, String description) throws AbstractException;

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractException;

    Task updateById(String userId, String id, String name, String description) throws AbstractException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusById(String userId, String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

}
