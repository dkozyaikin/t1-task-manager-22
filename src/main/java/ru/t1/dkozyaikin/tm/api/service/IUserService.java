package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword (String id, String password) throws AbstractException;

    User updateUser(String id, String firstName, String lastName, String middleName) throws AbstractException;

    Boolean isLoginExist(String login) throws AbstractException;

    Boolean isEmailExists(String email) throws AbstractException;

    void lockUserByLogin(String login) throws AbstractException;

    void unlockUserByLogin(String login) throws AbstractException;

}
