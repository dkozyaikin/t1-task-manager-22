package ru.t1.dkozyaikin.tm.api.service;

import ru.t1.dkozyaikin.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String taskId, String projectId) throws AbstractException;

    void removeProjectById(String userId, String projectId) throws AbstractException;

    void removeProjectByIndex(String userId, Integer projectIndex) throws AbstractException;

    void clearAllProjects(String userId) throws AbstractException;

    void unbindTaskFromProject(String userId, String taskId, String projectId) throws AbstractException;

}

