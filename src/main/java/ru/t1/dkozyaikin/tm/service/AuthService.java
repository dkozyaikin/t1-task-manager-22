package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.service.IAuthService;
import ru.t1.dkozyaikin.tm.api.service.IUserService;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.field.LoginEmptyException;
import ru.t1.dkozyaikin.tm.exception.field.PasswordEmptyException;
import ru.t1.dkozyaikin.tm.exception.user.AccessDeniedException;
import ru.t1.dkozyaikin.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.dkozyaikin.tm.exception.user.PermissionException;
import ru.t1.dkozyaikin.tm.exception.user.UserLockedException;
import ru.t1.dkozyaikin.tm.model.User;
import ru.t1.dkozyaikin.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) throws AbstractException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new UserLockedException();
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public Boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() throws AbstractException {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(final Role[] roles) throws AbstractException {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
