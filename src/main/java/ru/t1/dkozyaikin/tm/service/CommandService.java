package ru.t1.dkozyaikin.tm.service;

import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.service.ICommandService;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozyaikin.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) throws AbstractException {
        if (command == null) throw new CommandNotSupportedException();
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) throws AbstractException {
        if (argument == null || argument.isEmpty()) throw  new ArgumentNotSupportedException();
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void clearOutput() {
        System.out.print("\033\143");
    }

}
